import { BaseDto } from '../models/base.dto';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map, switchMap, catchError } from 'rxjs/operators';
import { BaseConfiguration } from './app-configuration';
import { AppConfigurationService } from './app-configuration.service';

export abstract class BaseHttpService {
    private config$: Observable<BaseConfiguration>;

    protected constructor(
        protected http: HttpClient,
        protected appConfigurationService: AppConfigurationService
    ) {
        this.config$ = this.appConfigurationService.getConfiguration();
    }

    protected createOptions(method: string, httpParams: HttpParams = null) {
        const httpHeaders = new HttpHeaders({
            'Content-Type': 'application/json'
        });

        const options = (httpParams) ? {
            headers: httpHeaders,
            method: method,
            params: httpParams
        } : {
                headers: httpHeaders,
                method: method
            }

        return options;
    }

    protected get<T>(relativeUrl: string): Observable<T> {
        const url$: Observable<string> = this.getUrl(relativeUrl);
        return url$
            .pipe(
                switchMap((url: string) => this.http.get<BaseDto<T>>(url)),
                map(this.toBaseDto),
                map(this.extractResult)
            );
    }

    protected getEx<T>(relativeUrl: string, httpOptions: any): Observable<T> {
        const url$: Observable<string> = this.getUrl(relativeUrl);
        return url$
            .pipe(
                switchMap((url: string) => this.http.get<BaseDto<T>>(url, httpOptions)),
                map(this.toBaseDto),
                map(this.extractResult)
            );
    }

    protected post<T>(relativeUrl: string, body: any): Observable<T> {
        const url$: Observable<string> = this.getUrl(relativeUrl);
        return url$
            .pipe(
                catchError(error => {
                    console.log(error);
                    throw error;
                }),
                switchMap((url: string) => this.http.post<BaseDto<T>>(url, body)),

                map(this.extractResult)
            );
    }

    protected postEx<T, S>(relativeUrl: string, httpOptions: any, body?: S): Observable<T> {
        const url$: Observable<string> = this.getUrl(relativeUrl);
        return url$
            .pipe(
                switchMap((url: string) => {
                    if (body) {
                        return this.http.post<BaseDto<T>>(url, body, httpOptions)
                    } else {
                        return this.http.post<BaseDto<T>>(url, httpOptions)
                    }
                }),
                map(this.toBaseDto),
                map(this.extractResult)
            );
    }

    protected put<T>(relativeUrl: string, body: any): Observable<T> {
        const url$: Observable<string> = this.getUrl(relativeUrl);
        return url$
            .pipe(
                switchMap((url: string) => this.http.put<BaseDto<T>>(url, body)),
                map(this.extractResult)
            );
    }

    protected putEx<T, S>(relativeUrl: string, httpOptions: any, body: S): Observable<T> {
        const url$: Observable<string> = this.getUrl(relativeUrl);
        return url$
            .pipe(
                switchMap((url: string) => this.http.put<BaseDto<T>>(url, body, httpOptions)),
                map(this.toBaseDto),
                map(this.extractResult)
            );
    }

    protected deleteEx<T>(relativeUrl: string, httpOptions: any): Observable<T> {
        const url$: Observable<string> = this.getUrl(relativeUrl);
        return url$
            .pipe(
                switchMap((url: string) => this.http.delete<BaseDto<T>>(url, httpOptions)),
                map(this.toBaseDto),
                map(this.extractResult)
            )
    }

    protected deleteById<T>(relativeUrl: string): Observable<T> {
        const url$: Observable<string> = this.getUrl(relativeUrl);
        return url$
            .pipe(
                switchMap((url: string) => this.http.delete<BaseDto<T>>(url)),
                map((response) => {
                    return response.result;
                })
            )
    }

    protected getUrl(relativeUrl: string): Observable<string> {
        return this.config$
            .pipe(
                map((config: BaseConfiguration) => {
                    return config.getAbsoluteUrl(relativeUrl);
                })
            );
    }

    // takes in response of type any or BaseDto<T> or HttpResponse<BaseDto<T>>
    // force response to type BaseDto<T>
    private toBaseDto(dto: any): BaseDto<any> {
        let baseDto: BaseDto<any>;

        if (dto.result === undefined && dto.body === undefined && dto !== undefined) {
            baseDto = { result: dto };
            return baseDto;
        }
        else if (dto.body) {
            baseDto = { result: dto.body.result };
            return baseDto;
        }
        return dto;
    }

    private extractResult<T>(baseDto: BaseDto<T>): T {
        return baseDto.result;
    }
}