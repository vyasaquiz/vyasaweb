import { isDevMode } from '@angular/core';

export interface AppConfiguration {
    baseUrl: string;
    socialMediaBaseUrl: string;
    businessUnit: string;
    isDevEnvironment: boolean;
    cssThemeClassName: string;
    logoPath: string;
    favIcon: string;
    appBrowserTabLabel: string;
    showHeaderMoreMenu: boolean;
}

export class BaseConfiguration implements AppConfiguration {
    baseUrl: string;
    socialMediaBaseUrl: string;
    businessUnit: string;
    isDevEnvironment: boolean;
    cssThemeClassName: string;
    logoPath: string;
    favIcon: string;
    appBrowserTabLabel: string;
    showHeaderMoreMenu: boolean;
    constructor(config: AppConfiguration) {
        this.appBrowserTabLabel = config.appBrowserTabLabel,
            this.baseUrl = config.baseUrl,
            this.businessUnit = config.businessUnit,
            this.cssThemeClassName = config.cssThemeClassName,
            this.favIcon = config.favIcon,
            this.isDevEnvironment = config.isDevEnvironment,
            this.logoPath = config.logoPath,
            this.showHeaderMoreMenu = config.showHeaderMoreMenu,
            this.socialMediaBaseUrl = config.socialMediaBaseUrl
    }

    getAbsoluteUrl(relativeUrl: string): string {
        const urlParts = relativeUrl.split('/');
        let apiName = '';
        if (urlParts.length > 1 && urlParts[1] === 'api') {
            apiName = urlParts[2]; // e.g. /api/organization/users
        } else {
            apiName = urlParts[1]; // e.g. /user
        }
        const baseUrl = this.getBaseUrl(apiName);
        return baseUrl + relativeUrl;
    }

    getBaseUrl(apiName: string): string {
        return apiName;
    }
}

export class EmptyConfiguration implements AppConfiguration {
    constructor(
        public baseUrl = '',
        public socialMediaBaseUrl = '',
        public businessUnit = '',
        public isDevEnvironment = false,
        public cssThemeClassName = '',
        public logoPath = '',
        public favIcon = '',
        public appBrowserTabLabel = '',
        public showHeaderMoreMenu = false,
    ) { }
}