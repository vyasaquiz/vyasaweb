import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AppConfiguration, BaseConfiguration, EmptyConfiguration } from './app-configuration';

@Injectable({ providedIn: 'root' })
export class AppConfigurationService implements OnDestroy {
    
    private readonly configurationSubject: ReplaySubject<BaseConfiguration>;
    private configuration: BaseConfiguration;

    constructor(private httpClient: HttpClient) {
        this.configuration = new BaseConfiguration(new EmptyConfiguration());
        this.configurationSubject = new ReplaySubject<BaseConfiguration>(1);
    }

    ngOnDestroy(): void {
        this.configurationSubject.complete();
    }

    getConfiguration(): Observable<BaseConfiguration> {
        return this.configurationSubject.asObservable();
    }

    loadConfigurationData(url: string): Promise<AppConfiguration> {
        return this.httpClient
            .get<AppConfiguration>(url)
            .pipe(
            tap((config: AppConfiguration) => {
                this.configuration = new BaseConfiguration(config);
                this.configurationSubject.next(this.configuration);
            })
            )
            .toPromise();
        }
}
