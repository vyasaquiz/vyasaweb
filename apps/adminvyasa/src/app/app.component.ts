import { Component } from '@angular/core';

@Component({
  selector: 'vyasa-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'adminvyasa';
}
